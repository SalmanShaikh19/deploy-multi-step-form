import React from "react";
import "../css/Message.css";
import Navbar from "./Navbar.js";
import messageImage from "../assets/form2.webp";


function Message({ data }) {

  const setdata =(e)=>{
    let value=e.target.value
  
    switch(e.target.id){
      case "message":
        if (value.length) {
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              messageComplete: true,
              message: value,
            }
          })
        }else{
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              messageComplete: false,
              message: value,
            }
          })
        }
        break;

      case "optionOne":
        data.setInitialValues((previousValues)=>{
          return{
            ...previousValues,
            optionSelected: 1,
          }
        })
        break;

      case "optionTwo":
        data.setInitialValues((previousValues)=>{
          return{
            ...previousValues,
            optionSelected: 2,
          }
        })
        break;

      default:
        console.log("no ids matching");
        break;
      }

  }



  return (
    <section className="messageOuterContainer">

    <div className="messageInnerContainer">
   
      <div className="messageImageContainer">
        <img className="imageTwo" src={messageImage} alt="messageImage"/>
      </div>


      <div className="messageRightSide">
    
      <Navbar stepTwo={data.step}/>

      <div className="messageHeadingsContainer">
      <p className="stepCounter">step 2/3</p>
          <h2 className="messageHeading">Message</h2>
      </div>

      <div className="messageForm">
        <label className="flexColumnLabel">message
        <textarea className={data.initialValues.messageComplete=== false? "redBorder messageTextArea":"messageTextArea"} id="message" value={data.initialValues.message} onChange={setdata}/>
        {data.initialValues.messageComplete === false && <p className="error">Please fill this field</p>}
        </label>
      </div>

      <div className="radioButtonContainer c">
      <label className="paddingBottom cursorPointer">
            <input type="radio"  checked={data.initialValues.optionSelected === 1? true:false} name="two" id="optionOne" className="radioButtons" value="1" onChange={setdata}/>
            The number one choice
          </label>
          <label className="cursorPointer">
            <input type="radio" name="two" checked={data.initialValues.optionSelected === 2? true:false} className="radioButtons" id="optionTwo"  value="0" onChange={setdata}/>
            The number two choice
          </label>
      </div>

      <div className="buttonContainer">
      <button type="submit" className="backButton" onClick={data.goToBack}>Back</button>
      <button className="buttonTwo" onClick={data.goToNextStep}>Next Step</button>
      </div>
      </div>
  
    </div>
  
  </section>
  );
}

export default Message;
