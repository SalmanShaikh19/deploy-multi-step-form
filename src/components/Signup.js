import React from "react";
import signupImage from "../assets/form1.webp";
import Navbar from "./Navbar.js"
import '../css/Signup.css';




function Signup({data}) {

  const saveChange = (e) => {
    let value = e.target.value;
    switch(e.target.id){
      case "firstNameInput":
        if (
          !/\d/.test(value) &&
          value.length
        ) {
          data.setInitialValues((previousValues)=>{
            return {...previousValues,
            firstNameComplete:true,
            firstNameInput:value
            }
          })
        }else{
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              firstNameComplete:false,
              firstNameInput: value,
            }
          })
        }
        break;
      case "lastNameInput":
        if (
          !/\d/g.test(value) &&
          value.length
        ) {
          data.setInitialValues((previousValues)=>{
            return {...previousValues,
            lastNameComplete:true,
            lastNameInput:value
            }
          })
        }else{
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              lastNameComplete:false,
              lastNameInput: value,
            }
          })
        }
        break;
      
      case "dobInput":
        if (value.length) {
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              dobComplete: true,
              dobInput: value,
            }
          })
        }else{
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              dobComplete: false,
              dobInput: value,
            }
          })
        }
        break;
        
      case "emailInput":
        if( /\S+@\S+\.\S+/.test(value) &&
        value.length){
          data.setInitialValues((previousValues)=>{
            return {...previousValues,
            emailComplete:true,
            emailInput:value
            }
          })
        }else{
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              emailComplete:false,
              emailInput: value,
            }
          })
        }
        break;

      case "addressInput":
        if (value.length) {
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              addressComplete: true,
              addressInput: value,
            }
          })
        }else{
          data.setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              addressComplete: false,
              addressInput: value,
            }
          })
        }
        break;

      default:
        console.log("no id present in signup.js");
        break;
    }

  };

  return (
    <section className="outerContainer">

      <div className="innerContainer">

        <div className="signupImageContainer">
          <img className="imageOne" src={signupImage} alt="SignUpPageImage"/>
        </div>

        <div className="signupRightSide">

          <Navbar step={data.step}/>

          <div className="signupHeadingsContainer">
          <p className="stepCounter">step 1/3</p>
          <h2 className="signupHeading">Sign UP</h2>
          </div>

          <form className="signupForm">
            <div className="firstRowForm">
              <label className="firstName flexColumn">
                First Name
                <input type="text" className={data.initialValues.firstNameComplete === false ? "redBorder inputField": "inputField"} id="firstNameInput" value={data.initialValues.firstNameInput} onChange={saveChange} required="required"/>
                {data.initialValues.firstNameComplete === false && <p className="error">Name should not contain numbers </p>}
              </label>
              <label className="lastName flexColumn">
                Last Name
                <input type="text" className={data.initialValues.lastNameComplete === false ? "redBorder inputField lastName": "inputField lastName"}  id="lastNameInput" value={data.initialValues.lastNameInput} onChange={saveChange} required="required"/>
                {data.initialValues.lastNameComplete === false && <p className="error">Name should not contain numbers </p>}
              </label>
            </div>

            <div className="secondRowForm">
              <label className="flexColumn">
                Date of Birth
               <input type="date"  className={data.initialValues.dobComplete === false ? "redBorder inputFieldDob": "inputFieldDob"}  required="required"  id="dobInput"  value={data.initialValues.dobInput} onChange={saveChange}/>
               {data.initialValues.dobComplete === false && <p className="error">Please fill this field</p>}

              </label>
              <label className="flexColumn">
                Email-Address
                <input type="email" className={data.initialValues.emailComplete === false ? "redBorder inputField": "inputField"}  id="emailInput" value={data.initialValues.emailInput} onChange={saveChange} required="required" />
                {data.initialValues.emailComplete === false && <p className="error">Please provide valid email id</p>}

              </label>
            </div>

            <label className="flexColumn address">
              Address
              <input type="text" className={data.initialValues.addressComplete === false ? "redBorder inputFieldAddress": "inputFieldAddress"}  id="addressInput" value={data.initialValues.addressInput} onChange={saveChange}  required="required"/>
              {data.initialValues.addressComplete === false && <p className="error">Please fill this field</p>}

            </label>
            
            <button className="buttonOne" onClick={data.goToNextStep}>Next Step</button>
          </form>
        </div>
      </div>
    </section>
  );
}

export default Signup;
