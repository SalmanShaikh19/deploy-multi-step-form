import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import "../css/Navbar.css"

function Navbar({step,stepTwo,stepThree}) {

 
  return (
    <>
    <nav className="signupNav">
    <p className="flexbox"> 
    <FontAwesomeIcon icon={faCheck} className={stepTwo===2 || stepThree===3?"show":"hide"} />
      <span className={stepTwo===2 || stepThree===3?"hide":"number"} id={step===1?"backgroundBlue":null}>1</span>Sign Up
    </p>
    <p className="flexbox">
        <FontAwesomeIcon icon={faCheck} className={stepThree===3?"show":"hide"} />
      <span className={stepThree===3?"hide":"number"} id={stepTwo===2?"backgroundBlue":null}>2</span> Message
    </p>
    <p className="flexbox">
    
      <span className="number" id={stepThree===3?"backgroundBlue":null}>3</span>Checkbox
    </p>
  </nav>
  </>
  )
}

export default Navbar