import Signup from "./Signup";
import Message from "./Message.js";
import Checkbox from "./Checkbox.js";
import React, { useState} from "react";

function App() {
  const [step, setStep] = useState(1);


  const [initialValues, setInitialValues] = useState({
    firstNameInput: "",
    firstNameComplete: null,
    lastNameInput: "",
    lastNameComplete: null,
    emailInput: "",
    emailComplete: null,
    dobInput: "",
    dobComplete: null,
    addressInput: "",
    addressComplete: null,
    message:"",
    messageComplete: null,
    optionSelected:1,
    foodSelected: "burger",
    checkboxFirstOption: false,
    checkboxSecondOption: false,
  });

  const submit=(e)=>{
    alert(`
    firstName: ${initialValues.firstNameInput}\n
    lastName: ${initialValues.lastNameInput}\n
    email: ${initialValues.emailInput}\n
    dob: ${initialValues.dobInput}\n
    message: ${initialValues.message}\n
    optionSelected: ${initialValues.optionSelected ===1? "The number one choice":"The number two choice"}\n
    foodSelected: ${initialValues.foodSelected}`)
  }


  const goToNextStep = (e) => {
    e.preventDefault();

    
    switch(step){
      case 1:
        if (initialValues.firstNameComplete && initialValues.lastNameComplete && initialValues.emailComplete && initialValues.dobComplete && initialValues.addressComplete){
          setStep(step+1)
        }else{
          setInitialValues((previousValues)=>{
            return {
              ...previousValues,
              firstNameComplete: initialValues.firstNameComplete === null? false: initialValues.firstNameComplete,
              lastNameComplete: initialValues.lastNameComplete === null? false: initialValues.lastNameComplete,
              emailComplete: initialValues.emailComplete === null? false: initialValues.emailComplete,
              dobComplete: initialValues.dobComplete === null? false: initialValues.dobComplete,
              addressComplete:initialValues.addressComplete === null? false: initialValues.addressComplete
            }
          })
        }
        break;
        
      case 2:
        if (initialValues.messageComplete){
          setStep(step+1)
        }else{
          setInitialValues((previousValues)=>{
            return{
              ...previousValues,
              messageComplete: initialValues.messageComplete === null? false: initialValues.messageComplete
            }
          })
        }
        break;

      default:
        console.log("no such step");
        break;
    }  
  };

  const goToBack =(e)=>{
    setStep(step-1)
  }


  const showStep = () => {
    switch (step) {
      case 1:
        return (
          <Signup
            data={{
              step: step,
              initialValues: initialValues,
              setInitialValues: setInitialValues,
              goToNextStep: goToNextStep,
            }}
          />
        );

      case 2:
        return (
          <Message
            data={{
              step: step,
              initialValues: initialValues,
              setInitialValues: setInitialValues,
              goToNextStep: goToNextStep,
              goToBack: goToBack
            }}
          />
        );

        case 3:
          return(
            <Checkbox
            data={{
              step: step,
              initialValues: initialValues,
              setInitialValues: setInitialValues,
              submit:submit,
              goToBack: goToBack
            }}
          />
          )
      default:
        return <div>No components</div>;
    }
  };

  return(
    <> 
    <form onSubmit={submit}>
    {showStep()}
    </form>
    </>
   
  )
}

export default App;
