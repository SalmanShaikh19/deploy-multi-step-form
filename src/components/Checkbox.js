import React from "react";
import Navbar from "./Navbar.js";
import checkboxImage from "../assets/form3.webp";
import "../css/Checkbox.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBurger } from "@fortawesome/free-solid-svg-icons";
import { faFish } from "@fortawesome/free-solid-svg-icons";

function Checkbox({ data }) {
 
  const setdata=(e)=>{
    console.log(e.target.value)
    switch(e.target.id){
      case "burger":
        data.setInitialValues((previousValues)=>{
          return{
            ...previousValues,
            foodSelected: "burger",
          }
        })
        break;

      case "fish":
        data.setInitialValues((previousValues)=>{
          return{
            ...previousValues,
            foodSelected: "fish",
          }
        })
        break;

     case "checkboxFirstOption":
        data.setInitialValues((previousValues)=>{
          return{
            ...previousValues,
            checkboxFirstOption: data.initialValues.checkboxFirstOption? false: true,
          }
        })
        break;

      case "checkboxSecondOption":
        data.setInitialValues((previousValues)=>{
          return{
            ...previousValues,
            checkboxSecondOption: data.initialValues.checkboxSecondOption? false: true,
          }
        })
        break;

        default:
          console.log("no such id present in checkbox.js")

    }   
  }

  return (
    <>
      <section className="checkboxOuterContainer">
        <div className="checkboxInnerContainer">
          <div className="checkboxImageContainer">
            <img className="imageTwo" src={checkboxImage} alt="checkboxImage" />
          </div>

          <div className="checkboxRightSide">
            <Navbar stepThree={data.step} />

            <div className="checkboxHeadingsContainer">
              <p className="stepCounter">step 3/3</p>
              <h2 className="checkboxHeading">Checkbox</h2>
            </div>

            <div className="checkboxButtonContainer">
              <label className="radioButtonLabels">
                <input type="radio" name="test" value="small" id="burger" checked={data.initialValues.foodSelected === "burger"? true:false}  className="checkboxRadioInput" onChange={setdata}/>
                <FontAwesomeIcon icon={faBurger} className="checkboxButtons" />
              </label>
              <label className="radioButtonLabels">
                <input type="radio" name="test" value="small" id="fish" checked={data.initialValues.foodSelected === "fish"? true:false}  className="checkboxRadioInput" onChange={setdata}/>
                <FontAwesomeIcon icon={faFish} className="checkboxButtons" />
              </label>
            </div>

            <label className="checkboxLabel">
              <input type="checkbox" className="checkbox" id="checkboxFirstOption"  checked={data.initialValues.checkboxFirstOption? true:false} onChange={setdata}/>
              <span className="checkboxPara">I want to add this option</span>
            </label>
            <label className="checkboxLabel borderBottom">
              <input type="checkbox" className="checkbox" id="checkboxSecondOption" checked={data.initialValues.checkboxSecondOption? true:false} onChange={setdata} />
              <span className="checkboxPara">
                Let me click on this checkbox and choose some cool stuff
              </span>
            </label>

            <div className="checkboxButtonsContainer">
              <button type="submit" className="checkboxBackButton" onClick={data.goToBack}> Back </button>
              <button className="checkboxButtonTwo" type="submit">Submit</button>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Checkbox;
