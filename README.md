# Multi-Step-Form

### Structure and Working

1. public
    * It contains single `index.html` file which is having a single container with id root

2. src

    1. assets
        * This folder contains all the images used inside the project 
        * form1.webp is used in `Signup.js`
        * form2.webp is used in `Message.js`
        * form3.webp is used in `Checkbox.js`

    2. components
        * It contains all components used in project
        * `App.js` is parent component which is having three child components 
            * Signup.js
            * Message.js
            * Checkbox.js
        * Each of these three components have a child component `Navbar.js`
        * Only single component from above three is rendered by using switch statement which takes step as parameter, step is the number which will increment as user clicks on nextStep in form 
        * All states are handeled inside `App.js` and passed as props to child components

    3. css
        * All css files are stored here with same names as of components
            * Signup.css
            * Message.css
            * Checkbox.css
            * Navbar.css

    4. index.js
        * This file mounts the main component `App.js` onto the root element in `index.html` file
    
    

## Setup the project

Run this command to install dependencies 
   
    npm install   

Now run

    npm start

This will start the live server


